package com.army;

import java.util.Objects;

import com.army.exceptions.FailedSpellRequirementsException;
import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.InvalidUnitMountTypeException;
import com.army.exceptions.InvalidUnmountException;
import com.army.exceptions.UnitIsDeadException;

@FunctionalInterface
public interface CheckedBiConsumer<T, R> {
	void accept(T t, R r) throws UnitIsDeadException, InvalidRangeException, FailedSpellRequirementsException,
		InvalidUnitMountTypeException, InvalidUnmountException;

	public default CheckedBiConsumer<T, R> andThen(CheckedBiConsumer<? super T, ? super R> after) { 
		Objects.requireNonNull(after);
		return (a, b) -> {
			accept(a, b);
			after.accept(a, b);
		};
	}
}