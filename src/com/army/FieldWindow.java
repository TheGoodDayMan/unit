package com.army;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Event;
import java.awt.Choice;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;
import javax.xml.ws.spi.Invoker;

import com.army.exceptions.FailedSpellRequirementsException;
import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.InvalidUnitMountTypeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.units.Mountable;
import com.army.units.Unit;
import com.army.units.melee.Warrior;
import com.army.units.melee.Werewolf;

import java.awt.GridLayout;
import java.awt.MouseInfo;
import java.awt.TextField;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class FieldWindow extends JFrame {

	private JPanel contentPane;
	
	public static Location[][] locations;
	
	public static int gridSize;
	
	JButton[][] field;

	CheckedConsumer<Location> executorOnLoc = (x) -> {
		if(x.getUnit() instanceof Mountable) {
			getMountPickList(x.getUnit(), (y) -> {getDropdownMenuFromUnit(y);});
		} else if(x.getUnit()!=null) getDropdownMenuFromUnit(x.getUnit());
	};
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FieldWindow frame = new FieldWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void resetExecutor() {
		executorOnLoc = (x) -> {
			if(x.getUnit() instanceof Mountable) {
				getMountPickList(x.getUnit(), (y) -> {getDropdownMenuFromUnit(y);});
			} else if(x.getUnit()!=null) getDropdownMenuFromUnit(x.getUnit());
		};
	}
	
	public void updateButtons() {
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				//field[i][j].setIcon(new ImageIcon(locations[i][j].getGraphics()));
				ImageIcon temp = new ImageIcon(locations[i][j].getGraphics());
					//.getScaledInstance(field[i][j].getWidth(), field[i][j].getHeight(), java.awt.Image.SCALE_DEFAULT));
				field[i][j].setIcon(temp);
				if(locations[i][j].getUnit() != null ) {
					field[i][j].setToolTipText(locations[i][j].getUnit().toString());
				} else {
					field[i][j].setToolTipText(String.format("%d %d", i,j));
				}
			}
		}
		//System.out.println(field[0][0].getWidth());
	}

	/**
	 * Create the frame.
	 */
	public FieldWindow() {
		Unit.setWindow(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		this.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				updateButtons();
			}

			//I don't need there, *shrug* but thanks oracle, I need to write that.
			@Override
			public void windowClosed(WindowEvent arg0) {}

			@Override
			public void windowClosing(WindowEvent arg0) {}

			@Override
			public void windowDeactivated(WindowEvent arg0) {}

			@Override
			public void windowDeiconified(WindowEvent arg0) {}

			@Override
			public void windowIconified(WindowEvent arg0) {}

			@Override
			public void windowOpened(WindowEvent arg0) {}
		});
		
		
		field = new JButton[8][8];
		locations = new Location[8][8];
		contentPane.setLayout(new GridLayout(8, 8, 0, 0));
		
		JMenuBar menuBar = new JMenuBar();
		//contentPane.add(menuBar);
		this.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Units");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Add...");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JDialog unitMaker = new JDialog();
				unitMaker.setTitle("Add a unit");
				JPanel contentForDialog;
				
				contentForDialog = new JPanel();
				contentForDialog.setBorder(new EmptyBorder(5, 5, 5, 5));
				contentForDialog.setLayout(new BoxLayout(contentForDialog, BoxLayout.Y_AXIS));
				
				unitMaker.setContentPane(contentForDialog);
				unitMaker.setSize(100,100);
				
				TextField unitName = new TextField("name");
				unitMaker.getContentPane().add(unitName);
				
				Button addUnit = new Button("Add");
				
				Choice units = new Choice();
				
				//Melee units
				units.add("melee.Warrior");
				units.add("melee.Werewolf");
				units.add("melee.Vampire");
				units.add("melee.Berserker");
				units.add("melee.Rogue");
				
				//Spellcaster units
				units.add("spellcasters.Healer");
				units.add("spellcasters.Priest");
				units.add("spellcasters.Necromancer");
				units.add("spellcasters.Wizard");
				units.add("spellcasters.Warlock");
				
				//Ranged
				units.add("long_range.Archer");
				
				//Mounts (TODO mounts)
				units.add("mountable.Horse");
				
				unitMaker.getContentPane().add(units);
				
				//TODO Completely redo this.
				addUnit.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							Class<?> unitType = Class.forName("com.army.units."+units.getSelectedItem());
							Constructor<Unit> maker;
							try {
								maker = (Constructor<Unit>) unitType.getConstructor(String.class);
								executorOnLoc = x -> {try {
									if(x.getUnit() == null) {
									x.setUnit((Unit)maker.newInstance(unitName.getText()));
									x.getUnit().setLocation(x);
									} else {
										JOptionPane.showMessageDialog(contentPane, "Please, choose an empty cell");
									}
								} catch (Exception e1) {
									e1.printStackTrace();
								}};
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							executorOnLoc = executorOnLoc.andThen((y) ->  {
								resetExecutor();
								setCursorPerAction("PICK");
								updateButtons();
								repaint();
							;});
							
							unitMaker.setVisible(false);
							updateButtons();
							repaint();
						} catch (ClassNotFoundException e) {
							JOptionPane.showMessageDialog(unitMaker, "Error finding class");
						}
					}
				});
				unitMaker.getContentPane().add(addUnit);
				
				unitMaker.setVisible(true);
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Remove...");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Restart field");
		mnNewMenu.add(mntmNewMenuItem_2);
		
		
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				try {
					locations[i][j] = new Location(i,j,null,null,null,null);
				} catch (IOException ioe) {
					JOptionPane.showMessageDialog(this, "ground.jpg not there?");
				}
				field[i][j] = new JButton(String.format("%d %d",i,j));
				field[i][j].setBackground(Color.green);
				
				field[i][j].addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						String[] locs = (e.getActionCommand().split(" "));
						int x = Integer.parseInt(locs[0]);
						int y = Integer.parseInt(locs[1]);
						
						try {
						executorOnLoc.accept(locations[x][y]);
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(getOwner(), e1.getMessage());
							//TODO refactor of this , this is repeated 3 times now (as I write this comment)
							resetExecutor();
							setCursorPerAction("PICK");
						}
						updateButtons();
						repaint();
						System.out.println(locations[x][y].getUnit() == null? 
								String.format("%d, %d", x,y) : locations[x][y].getUnit().toString());
						//TODO
						//executorOnLoc = default dropdown thingy 
					}
					
				});
				
				contentPane.add(String.format("%d %d",i,j), field[i][j]);
			}
		}
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				locations[i][j].setEast(j < 7 ? locations[i][j+1] : null);
				locations[i][j].setWest(j > 0 ? locations[i][j-1] : null);
				locations[i][j].setNorth(i < 7 ? locations[i+1][j] : null);
				locations[i][j].setSouth(i > 0 ? locations[i-1][j] : null);
			}
		}
		
	}

	
	public void getDropdownMenuFromUnit(Unit u) {
		Map<String, CheckedConsumer<Location> > commands = u.getCommands();
		
		JPopupMenu menu = new JPopupMenu();
		
		for(Map.Entry<String, CheckedConsumer<Location>> entry : commands.entrySet()) {
			JMenuItem temp = new JMenuItem();
			CheckedConsumer<Location> executorOnit = entry.getValue();
			temp.addActionListener( new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					//Assign executor to our stuff.
					executorOnLoc = executorOnit;
					
					//Assign the dropdown menu fetch back after action is done.
					executorOnLoc = executorOnLoc.andThen((y) ->  {
						resetExecutor();
						setCursorPerAction("PICK");
						updateButtons();
						repaint();
						;});
					
					//Remove the dropdown after done.
					menu.setVisible(false);
					
					//Set cursor
					setCursorPerAction(entry.getKey());
				}
			});
			temp.setName(entry.getKey());
			temp.setText(temp.getName());
			menu.add(temp);
		}
		menu.setMinimumSize(new Dimension(100,30*commands.size()));
		menu.setPopupSize(100, 30*commands.size());
		menu.pack();
		menu.show(getParent(), MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo().getLocation().y);
		menu.setVisible(true);
		//TODO add menus that assign a new executor to existing one
	}
	
	public void getMountPickList(Unit u, CheckedConsumer<Unit> executorOnUnit) {
		List<Unit> units = ((Mountable)u).getMountersList();
		
		JPopupMenu menu = new JPopupMenu();
		
		JMenuItem temp = new JMenuItem();
		temp.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					executorOnUnit.accept(u);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(contentPane, e1.getMessage());
				} 
				menu.setVisible(false);
			}
		});
		temp.setText(u.getName() + " - " + u.getClass().getSimpleName());
		menu.add(temp);
		
		for(Unit unit : units) {
			JMenuItem tmp = new JMenuItem();
			tmp.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						executorOnUnit.accept(unit);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(contentPane, e1.getMessage());
					}
					menu.setVisible(false);
				}
			});
			tmp.setText(unit.getName() + " - " + unit.getClass().getSimpleName());
			menu.add(tmp);
		}
		
		menu.setMinimumSize(new Dimension(100,30*menu.getComponentCount()));
		menu.setPopupSize(100, 30*menu.getComponentCount());
		menu.pack();
		menu.show(getParent(), MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo().getLocation().y);
		menu.setVisible(true);
	}
	
	public void setCursorPerAction(String action) {
		switch (action) {
		case "ATTACK" : {this.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR)); break;}
		case "MOVE" : {this.setCursor(new Cursor(Cursor.MOVE_CURSOR)); break;}
		case "PICK" : {this.setCursor(new Cursor(Cursor.HAND_CURSOR)); break;}
		default : {this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); break;}
		}
			
	}
	
	
}