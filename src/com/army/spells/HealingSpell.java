package com.army.spells;

import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.status_effects.Reasons;
import com.army.units.SpellCaster;
import com.army.units.Unit;

public abstract class HealingSpell extends Spell implements Castable {

	protected double undamage;
	
	public HealingSpell(String name, int whiteAffinity, double udamage, double manaCons, double rng, SpellNames sname) {
		super(name, whiteAffinity, 0, 0, 0, manaCons, rng, sname);
		this.undamage = udamage;
	}
	
	@Override
	public void cast(SpellCaster caster, Unit target) throws UnitIsDeadException, InvalidRangeException {
		super.cast(caster, target);
		caster.isAlive();
		caster.getStats().considerEffects(Reasons.ACTION);
		target.isAlive();
		caster.getStats().considerEffects(Reasons.CAST);
		double effect = caster.considerPower(this, undamage);
		//caster.spendMana(manaConsumption);
		//target.getStats().setHP(target.getStats().getHP()+effect);
		target.receiveMagicHeal(effect);
	}
}
