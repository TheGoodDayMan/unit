package com.army.spells;

/**
 * SpellNames
 * @author pavlo
 * Defines spell names used and available, for use in Maps that define a spellbook, also has the magic affinities
 * so app knows which spells a mage can't cast. Just here to reduce all that code rather, but we did that back then so.
 */
public enum SpellNames {
	FIREBALL(0,2,0,0),
	HEAL(2,0,0,0),
	NECROSTALK(0,4,0,1),
	SUMMONDEMON(0,6,0,0); //don't forget to remove the ; here
//	SPARK(0,3,0,0),
//	ICECUBE(0,3,0,0); 
	
	int reqWhite;	//Requirements for magic affinity. Healers have white, necro has black, wizards have what?
	int reqBlack;	//Kinda, YOU assign what magic wizard knows? These 4 values should be balanced for what
	int reqGreen;	//White heals, black damages, green buffs, blue does mindtricks - e.g, inherits spell.
	int reqBlue;
	
	private SpellNames(int reqW, int reqB, int reqG, int reqBl) {
		reqWhite = reqW;
		reqBlack = reqB;
		reqBlue = reqBl;
		reqGreen = reqG;
	}
}

