package com.army.spells;

import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.units.SpellCaster;
import com.army.units.Unit;


//I mean it would be cool to make it an interface but eh.

public abstract class Spell implements Castable {
	protected int reqWhite;	//Requirements for magic affinity. Healers have white, necro has black, wizards have what?
	protected int reqBlack;	//Kinda, YOU assign what magic wizard knows? These 4 values should be balanced for what
	protected int reqGreen;	//White heals, black damages, green buffs, blue does mindtricks - e.g, inherits spell.
	protected int reqBlue;
	
	protected String name; //ofc name for spell
	
	protected double manaConsumption;
	
	protected SpellNames spellInBook;
	
	protected double range;
	//Spell's power will be described in spell's desc. Spells to purge status effects don't need power. 
	//Necromancy neither
	
	public Spell(String newName, int rW, int rB, int rG, int rBl, double mana, double rangeOfSpell, 
			SpellNames spellInBook) {
		//Assign the values
		name = newName;
		reqWhite = rW;
		reqBlack = rB;
		reqGreen = rG;
		reqBlue = rBl;
		manaConsumption = mana;
		this.spellInBook = spellInBook;
		range = rangeOfSpell;
	}
	
	public SpellNames getNameInBook() {
		return spellInBook;
	}
	
	public double getManaCons() {
		return manaConsumption;
	}
	
	public double getRange() {
		return range;
	}
	
	public boolean affinitiesSatisfied(SpellCaster caster) {
		return( caster.getWhite() >= reqWhite && caster.getBlack() >= reqBlack && caster.getGreen() >= reqGreen 
				&& caster.getBlue() >= reqBlue);
	}
	
	//To be overloaded per Spell's nature. But is there just to debug(?)
	public String getReqsAsString() {
		return(String.format("Need level %d of white magic, level %d of black magic, level %d of green magic, "
				+ "level %d of blue magic", reqWhite, reqBlack, reqGreen, reqBlue));
	}
	
	//Overload this.
	public void cast(SpellCaster caster, Unit target) throws UnitIsDeadException, InvalidRangeException {
		if (this.getRange() < caster.getLocation().distanceTo(target.getLocation())) {
			throw new InvalidRangeException(this.getRange(), caster.getLocation().distanceTo(target.getLocation()));
		}
		//ZING
	}
}
