package com.army.spells;

import java.util.LinkedList;

import org.omg.CORBA.DynAnyPackage.Invalid;

import com.army.Location;
import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.units.SpellCaster;
import com.army.units.Unit;

public abstract class SummoningSpell extends Spell implements Castable {
	Unit summonedUnit;
	
	public SummoningSpell(String name, int blackAffinity, Unit summon, double manaCons, SpellNames sname, double rng) {
		super(name, 0, blackAffinity, 0, 0, manaCons, rng, sname);
		summonedUnit = summon;
	}
	
	public void cast(SpellCaster caster, Unit target) throws UnitIsDeadException, InvalidRangeException { 
		//Summoning spells add the unit to list of summoner's units. It can easily work because location would 
		//also refer the unit if e.g. warlock is dead, so the unit isn't GC'd
		super.cast(caster,target);
		try {
			if (caster.getSummonList() == null) {
				caster.initSummonList();				
			}
			Location onLoc = target.getLocation().nearAndEmpty();
			if (onLoc != null) {
			caster.getSummonList().add((Unit)(summonedUnit.clone()));
			caster.getSummonList().get(caster.getSummonList().size()-1).setLocation(onLoc);
			} else {
				//throw something, gonna refactor all over again...
			}
			//put it on board near target, perform check  before adding operation. is put on the 1 far cell;
		} catch (CloneNotSupportedException e) {  
			System.out.println("Apparently I can't clone that or any unit. Debug for details.");
		}
	}
}