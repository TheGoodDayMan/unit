package com.army.spells;

import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.units.SpellCaster;
import com.army.units.Unit;

public interface Castable {
	void cast (SpellCaster caster, Unit target) throws UnitIsDeadException, InvalidRangeException;
}
