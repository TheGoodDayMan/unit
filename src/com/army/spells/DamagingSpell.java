package com.army.spells;

import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.status_effects.Reasons;
import com.army.units.SpellCaster;
import com.army.units.Unit;

public abstract class DamagingSpell extends Spell implements Castable {
	
	protected double damage;
	
	public DamagingSpell(String name, int blackAffinity, double damage, double manaCons, double rng, SpellNames sname) {
		super(name, 0, blackAffinity, 0, 0, manaCons, rng, sname);
		this.damage = damage;
	}
	
	@Override
	public void cast(SpellCaster caster, Unit target) throws UnitIsDeadException, InvalidRangeException {
		super.cast(caster, target);
		caster.isAlive();
		caster.getStats().considerEffects(Reasons.ACTION);
		target.isAlive();
		caster.getStats().considerEffects(Reasons.CAST);
		double effect = caster.considerPower(this, damage);
		//caster.spendMana(manaConsumption);
		target.takeMagicDamage(effect);
	}
}
