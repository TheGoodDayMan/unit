package com.army.spells;

import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.units.SpellCaster;
import com.army.units.Unit;
//import com.army.units.Unit;
import com.army.units.melee.Demon;

public class SummonDemon extends SummoningSpell {
	
	static final double SUMMON_RANGE = 0;
	
	public SummonDemon() {
		super("Summon demon", 6, new Demon("Demon"), 40, SpellNames.SUMMONDEMON, SUMMON_RANGE); 
	}
	
	public void cast(SpellCaster caster) throws UnitIsDeadException, InvalidRangeException{ 
		super.cast(caster, caster); //is put near caster if used this way, probably can't be used properly because how 
		//spell caster works, but I'll leave it as option for maybe I figure out. I could also make it 0 farth spell to
		//only be cast on self as I implement board... probably that.
	}
}
