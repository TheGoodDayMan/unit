package com.army.spells;

public class Fireball extends DamagingSpell {
	
	final static double FIREBALL_RANGE = 3;
	final static double DEFAULT_DAMAGE = 35;
	final static double DEFAULT_COST = 10;
	
	public Fireball() {
		super("Fireball", SpellNames.FIREBALL.reqBlack, DEFAULT_DAMAGE, DEFAULT_COST, FIREBALL_RANGE, 
				SpellNames.FIREBALL);
	}
}
