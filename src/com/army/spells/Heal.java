package com.army.spells;

//Value for affinity taken from SpellNames



public class Heal extends HealingSpell {
	final static double HEAL_RANGE = 2;
	final static double DEFAULT_HEAL = 25;
	final static double DEFAULT_COST = 10;
	
	public Heal() {
		super("HEAL", SpellNames.HEAL.reqWhite, DEFAULT_HEAL, DEFAULT_COST, HEAL_RANGE, SpellNames.HEAL); 
		//Some hardcoded values
	}
}
