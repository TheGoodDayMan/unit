package com.army;

import java.util.*;
import java.util.function.Consumer;

import javax.imageio.ImageIO;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import com.army.units.Mountable;
import com.army.units.SpriteURLs;
import com.army.units.Unit;
import com.army.units.melee.Warrior; //temporary


public class Location {
	protected final Image ground  = SpriteURLs.GROUND.graphics;
	protected final int x;
	protected final int y;
	Unit unit;
	protected Location north; //reference
	protected Location east; //reference
	protected Location west; //reference
	protected Location south; //reference
	
	
	public Location(int onX, int onY, Location north, Location east, Location west, Location south) throws IOException {
		x = onX;
		y = onY;
		this.north = north;
		this.east = east;
		this.west = west;
		this.south = south;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public Unit getUnit() {
		return unit;
	}
	
	
	public Image getGraphics() {
		if (getUnit() == null) {
			return ground;
		} else {
			return getUnit().getGraphic();
		}
	}
	
	public Location getNorth() {
		return north;
	}
	
	public Location getSouth() {
		return south;
	}
	
	public Location getEast() {
		return east;
	}
	
	public Location getWest() {
		return west;
	}
	
	public void setNorth(Location north) {
		this.north = north;
	}

	public void setEast(Location east) {
		this.east = east;
	}

	public void setWest(Location west) {
		this.west = west;
	}

	public void setSouth(Location south) {
		this.south = south;
	}
	
	public void setUnit(Unit u) {
		unit = u;
	}

	public double distanceTo(Location loc) {
		return Math.hypot(Math.abs(x - loc.getX()), Math.abs(y - loc.getY()));
	}
	
	/**
	 * 
	 * @return null if no near cell is empty.
	 */
	public Location nearAndEmpty() {
		return(getNorth() != null && getNorth().getUnit() == null? getNorth() :
			getEast() != null && getEast().getUnit() == null? getEast() :
				getWest() != null && getWest().getUnit() == null? getWest() :
					getSouth() != null && getSouth().getUnit() == null? getSouth() :
						null);
	}
	
	
	
	public Map<String, Consumer<Location>> buildMenu() {
		Map<String, Consumer<Location>> g = new HashMap<String, Consumer<Location>>();
		if (unit == null) {
			g.put("Create unit", x -> {x.unit = new Warrior("new Unit");});
		}
		return g;
	}
}
