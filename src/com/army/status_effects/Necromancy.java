package com.army.status_effects;

import com.army.exceptions.UnitIsDeadException;
import com.army.units.Status;
import com.army.units.Unit;
import com.army.units.spellcasters.Necromancer;

public class Necromancy extends StatusEffect {
	Necromancer necro;
	
	public Necromancy(Necromancer nek, Unit target) {
		super(Reasons.DEATH, target.getStats());
		necro = nek;
	}
	
	@Override
	public void takeEffect(Status onStat) throws UnitIsDeadException {
		necro.getStats().setHP(necro.getStats().getHP()+(onUnitStat.getMaxHP()/10));
		necro.release(this);
	}

}
