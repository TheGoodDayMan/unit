package com.army.status_effects;

import com.army.exceptions.UnitIsDeadException;
import com.army.units.Status;

abstract public class StatusEffect {
	private Reasons reason;
	Status onUnitStat;
	
	public StatusEffect (Reasons r, Status stat) {
		reason = r;
		onUnitStat = stat;
	}
	
	public abstract void takeEffect(Status onStat) throws UnitIsDeadException; 
	
	public Reasons getReason(){
		return reason;
	}
}
