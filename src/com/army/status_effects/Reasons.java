package com.army.status_effects;

public enum Reasons {
	ACTION,
	MOVE,
	ATTACK,
	CAST,
	SHAPESHIFT,
	DEATH
}
