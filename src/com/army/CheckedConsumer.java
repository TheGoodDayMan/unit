package com.army;

import java.util.Objects;

import com.army.exceptions.FailedSpellRequirementsException;
import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.InvalidUnitMountTypeException;
import com.army.exceptions.InvalidUnmountException;
import com.army.exceptions.UnitIsDeadException;

@FunctionalInterface
public interface CheckedConsumer<T> {
	void accept(T t) throws UnitIsDeadException, InvalidRangeException, FailedSpellRequirementsException,
		InvalidUnitMountTypeException, InvalidUnmountException;

	public default CheckedConsumer<T> andThen(CheckedConsumer<? super T> after) { 
		Objects.requireNonNull(after);
		return (a) -> {
			accept(a);
			after.accept(a);
		};
	}
}