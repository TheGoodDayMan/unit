package com.army.units;

import java.util.ArrayList;
import java.util.List;

import com.army.Location;
import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.InvalidUnitMountTypeException;
import com.army.exceptions.InvalidUnmountException;
import com.army.exceptions.UnitIsDeadException;

public class Mountable extends Unit {
	protected List<Unit> mountedBy;
	protected int maxMounts;
	
	public Mountable(String name, double maxHP, double moveRng, int maxMnts) {
		super(name, null, maxHP);
		mountedBy = new ArrayList<Unit>();
		moveRange = moveRng;
		maxMounts = maxMnts;
	}
	
	public List<Unit> getMountersList() {
		return mountedBy;
	}
	
	public Location getLocation() {
		return onCell;
	}
	
	public void mount(Unit u) throws InvalidUnitMountTypeException {
		throw new InvalidUnitMountTypeException(u, this);
	}
	
	public boolean getMounted(Unit u) {
		if (!isFull()) {
			mountedBy.add(u);
			return true;
		} else return false;
	}
	
	public void getUnmounted(Unit u) {
		mountedBy.remove(u);
	}
	
	public void isAlive() throws UnitIsDeadException {
		if (stats.getHP() == 0) {
			for( Unit unit : mountedBy) {
				try {unit.unMount();}
				catch (InvalidUnmountException e1) {
					unit.setLocation(onCell);
				}
			}
			throw (new UnitIsDeadException(unitName));
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder()
					.append(getName())
					.append(": HP = ")
					.append(getStats().getHP())
					.append("/")
					.append(getStats().getMaxHP());
		for(Unit u : mountedBy) {
			sb.append("\n || ");
			sb.append(u.toString());
		}
		return sb.toString();
	}
	
	public boolean isFull() {
		return mountedBy.size() >= maxMounts;
	}
}
