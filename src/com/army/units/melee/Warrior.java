package com.army.units.melee;

import com.army.units.Unit;
//import com.army.weapons.Weapon;
import com.army.weapons.Sword;
import com.army.weapons.Weapon;

public class Warrior extends Unit {
	public Warrior(String name) {
		super(name, new Sword("Warrior's Sword"), 150); //Sword is a class in com.army.weapons
	}
	
	public Warrior(String name, Weapon wpn, double maxHP) {
		super(name, wpn, maxHP);
	}
}
