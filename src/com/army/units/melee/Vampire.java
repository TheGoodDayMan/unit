package com.army.units.melee;

import java.util.Map;
import java.util.function.BiConsumer;

import com.army.CheckedBiConsumer;
import com.army.CheckedConsumer;
import com.army.Location;
import com.army.exceptions.InvalidUnmountException;
import com.army.exceptions.UnitIsDeadException;
import com.army.units.Mountable;
import com.army.units.SpriteURLs;
import com.army.units.Unit;
import com.army.units.melee.Werewolf.Shapes;
import com.army.units.melee.Werewolf.extendedActions;
import com.army.weapons.Sword;
import com.army.weapons.Weapon;

public class Vampire extends Unit {
	
	protected enum extendedActions{
//			try {
//				if (y.getUnit()!= null) {
//				x.attack(y.getUnit());
//				((Vampire)x).convert(y.getUnit());
//				}
//			} catch (UnitIsDeadException e) {
//				//TODO TOSS A DIALOG
//			}
		ATTACK_AND_CONVERT((x,y) -> {
			if (y.getUnit() instanceof Mountable) {
				field.getMountPickList(y.getUnit(), (z) -> {
					x.attack(z);
					if (!(z instanceof Mountable)) {
						((Mountable) y.getUnit()).getMountersList().remove(z);
						z = ((Vampire)x).convert(z);
						((Mountable) y.getUnit()).getMountersList().add(z);
					}
				});
			} else if (y.getUnit() != null)
				x.attack(y.getUnit());
				((Vampire)x).convert(y.getUnit());
		});
		
		CheckedBiConsumer<Unit, Location> executor;
		
		extendedActions(CheckedBiConsumer<Unit, Location> act) {
			executor = act;
		}
	}
	
	public Vampire(String name) {
		super(name, new Sword("Dracula follower's sword"), 175);
		graphic = SpriteURLs.VAMPIRE.graphics;
	}
	
	public Vampire(String name, Weapon wpn, double mhp) {
		super(name, wpn, mhp);
		graphic = SpriteURLs.VAMPIRE.graphics;
	}
	
	public Map<String, CheckedConsumer<Location>> getCommands() {
		Map<String, CheckedConsumer<Location>> menu = super.getCommands();
		for (extendedActions entry : extendedActions.values()) {
			menu.put(entry.name(),(target)->{entry.executor.accept(this,target);});
		}
		
		return menu;
	}
	
	public Unit convert(Unit target) throws UnitIsDeadException, InvalidUnmountException {
		isAlive();
		target.isAlive();
		if (!(target instanceof Vampire || target instanceof Werewolf || target instanceof Mountable) ) {
			double currentHP = target.getStats().getHP();
			Location loc = target.getLocation();
			
			//target.unMount();
			target = new Vampire(target.getName(), target.getWeapon(), target.getStats().getMaxHP());
			target.getStats().setHP(currentHP);
			
			if(loc.getUnit() instanceof Mountable) {
				target.setMount((Mountable) loc.getUnit());
			} else target.setLocation(loc);
			
		}
		return target;
		//keep the image?
	}
	
	
}
