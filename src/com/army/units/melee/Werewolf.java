package com.army.units.melee;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import javax.imageio.ImageIO;
import javax.naming.ldap.ExtendedRequest;

import com.army.CheckedBiConsumer;
import com.army.CheckedConsumer;
import com.army.Location;
import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.InvalidUnitMountTypeException;
import com.army.exceptions.InvalidUnmountException;
import com.army.exceptions.UnitIsDeadException;
import com.army.status_effects.Reasons;
import com.army.units.Mountable;
import com.army.units.Unit;
import com.army.units.SpriteURLs;
import com.army.weapons.Weapon;
import com.army.weapons.Axe;
import com.army.weapons.Claws;

public class Werewolf extends Unit {
	public enum Shapes {
		MAN(1, 0, null, SpriteURLs.WEREWOLF_MAN.graphics),
		WEREWOLF(2, -1, new Claws(), SpriteURLs.WEREWOLF.graphics);
		
		double healthMult;
		double magicResistance;
		Weapon shapeWeap;
		Image image;
		
		Shapes(double hp, double mr, Weapon wpn, Image imageArg) { //Changed from String to Image
			healthMult = hp;
			magicResistance = mr;
			shapeWeap = wpn;
			try {
				image = imageArg;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	Shapes currentShape;
	
	protected enum extendedActions{
		ATTACK_AND_CONVERT((x,y) -> {
			if (y.getUnit() instanceof Mountable) {
				field.getMountPickList(y.getUnit(), (z) -> {
					x.attack(z);
					if (!(z instanceof Mountable)) {
						((Mountable) y.getUnit()).getMountersList().remove(z);
						z = ((Werewolf)x).convert(z);
						((Mountable) y.getUnit()).getMountersList().add(z);
					}
				});
			} else if (y.getUnit() != null)
				x.attack(y.getUnit());
				((Werewolf)x).convert(y.getUnit());
		});
		
		CheckedBiConsumer<Unit, Location> executor;
		
		extendedActions(CheckedBiConsumer<Unit, Location> act) {
			executor = act;
		}
	}
	
	public Werewolf(String name) {
		super(name, new Axe("Wildman's axe"), 150);
		currentShape = Shapes.MAN;
	}
	
	public Werewolf(String name, Weapon wpn, double mhp) {
		super(name, wpn, mhp);
		currentShape = Shapes.MAN;
	}
	
	public Map<String, CheckedConsumer<Location>> getCommands() {
		Map<String, CheckedConsumer<Location>> menu = super.getCommands();
		for (extendedActions entry : extendedActions.values()) {
			menu.put(entry.name(),(target)->{entry.executor.accept(this,target);});
		}
		
		for (Shapes entry : Shapes.values()) {
			menu.put(String.format("SHAPESHIFT INTO %s", entry.name()) , (x)->{this.changeShape(entry);});
		}
		return menu;
	}
	
	public void changeShape(Shapes shape) throws UnitIsDeadException{
		isAlive();
		
		if(currentShape != shape) {
			
		graphic = shape.image;
		
		getStats().setMaxHP(this.getStats().getMaxHP() * (shape.healthMult/currentShape.healthMult));
		
		magicResistance = shape.magicResistance;
		
		currentShape = shape;
		
		}
	}
	
	public Unit convert(Unit target) throws UnitIsDeadException, InvalidUnmountException {
		isAlive();
		target.isAlive();
		if (!(target instanceof Vampire || target instanceof Werewolf || target instanceof Mountable) ) {
			double currentHP = target.getStats().getHP();
			Location loc = target.getLocation();
			
			//target.unMount();
			target = new Werewolf(target.getName(), target.getWeapon(), target.getStats().getMaxHP());
			target.getStats().setHP(currentHP);
			
			if(loc.getUnit() instanceof Mountable) {
				target.setMount((Mountable) loc.getUnit());
			} else target.setLocation(loc);
			
		}
		return target;
		//keep the image?
	}
	
	public void attack(Unit target) throws UnitIsDeadException, InvalidRangeException {
		if (currentShape.shapeWeap != null) {
				currentShape.shapeWeap.getDamage();
				stats.considerEffects(Reasons.ACTION);
				isAlive();
				target.isAlive();
				if (currentShape.shapeWeap.getRange() < onCell.distanceTo(target.getLocation())) {
					throw new InvalidRangeException(currentShape.shapeWeap.getRange(), onCell.distanceTo(target.getLocation()));
				}
				stats.considerEffects(Reasons.ATTACK);
				target.takeDamage(currentShape.shapeWeap.getDamage(), this); 
			
		} else super.attack(target);
	}
	
	
	
	@Override
	public void takeDamage(double damage, Unit from) throws UnitIsDeadException {
		stats.setHP(stats.getHP()-damage); //stops there if dies, probably
		stats.considerEffects(Reasons.ATTACK);
		if (currentShape.shapeWeap != null) {
			from.getStats().setHP(from.getStats().getHP() - (currentShape.shapeWeap.getDamage()/2));
		} else if (wpn != null) {
			from.getStats().setHP(from.getStats().getHP()- (wpn.getDamage()/2));
		}
	}
}
