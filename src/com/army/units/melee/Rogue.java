package com.army.units.melee;

import com.army.exceptions.UnitIsDeadException;
import com.army.status_effects.Reasons;
import com.army.units.Unit;
import com.army.weapons.Dagger;

public class Rogue extends Unit {
	public Rogue(String name) {
		super(name, new Dagger("Rogue's dagger"),75);
	}
	
	@Override
	public void attack(Unit target) throws UnitIsDeadException { //Toss out as one of attack mechanics (separate class)
		stats.considerEffects(Reasons.ACTION);
		isAlive();
		target.isAlive();
		stats.considerEffects(Reasons.ATTACK);
		target.getStats().setHP(target.getStats().getHP() - getWeapon().getDamage()); //No counterAttack this way; 
	}
}
