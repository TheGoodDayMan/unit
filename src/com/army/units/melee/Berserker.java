package com.army.units.melee;

import com.army.weapons.Axe;
//import com.army.weapons.Sword;
//import com.army.exceptions.UnitIsDeadException;
import com.army.status_effects.StatusEffect;
import com.army.units.Unit;

public class Berserker extends Unit {
	public Berserker(String name) {
		super(name, new Axe("Berserker's axe", 35), 150); //Sword is a class in com.army.weapons
		magicResistance = 1;
	}
	
	@Override
	public void receiveMagicHeal(double undamage) {} //The feller is unaffected by this
	
	@Override
	public void receiveMagicStatus(StatusEffect se) {} // again
}
