package com.army.units.melee;

import com.army.units.SpriteURLs;
import com.army.weapons.Axe;

public class Demon extends Warrior {
	public Demon(String name) {
		super(name, new Axe("Demon's axe", 25), 100);
		graphic = SpriteURLs.WEREWOLF_MAN.graphics;
	}
}
