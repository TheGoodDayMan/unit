package com.army.units.spellcasters;

import com.army.exceptions.FailedSpellRequirementsException;
import com.army.spells.Fireball;
import com.army.spells.Heal;
import com.army.spells.Spell;
import com.army.spells.SpellNames;
import com.army.units.SpellCaster;
import com.army.weapons.Rod;

public class Wizard extends SpellCaster {
	public Wizard(String name) {
		super(name, new Rod("Wizzard's rod"), 100, 60, 2, 4, 0, 0 );
		try {
			addSpell(new Fireball());
//			addSpell(new Heal());
		}
		catch (FailedSpellRequirementsException e) {
			System.out.println("Contructor for wizard is not alrite :/");
		}
		setHealPower(0.5);
		setDamagePower(1);
	}
}
