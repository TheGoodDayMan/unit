package com.army.units.spellcasters;

import java.util.ArrayList;
import java.util.List;

import com.army.exceptions.FailedSpellRequirementsException;
import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.spells.Spell;
import com.army.status_effects.Necromancy;
import com.army.units.SpellCaster;
import com.army.units.SpriteURLs;
import com.army.units.Unit;
import com.army.weapons.DarkBook;
import com.army.weapons.Weapon;

public class Necromancer extends SpellCaster {
	List<Necromancy> stalks;
	
	public Necromancer(String name) {
		super(name, new DarkBook(), 120, 40, 0, 5, 0, 2);
		stalks = new ArrayList<Necromancy>();
		graphic = SpriteURLs.NECROMANCER.graphics;
	}
	
	public List<Necromancy> getStalksList() {
		return stalks;
	}
	
	public void release(Necromancy stalk) {
		stalks.remove(stalk);
	}
	
	public void attack(Unit target) throws UnitIsDeadException, InvalidRangeException { //Out as separate mechanic of damage (separate class)
		target.isAlive();
		Necromancy temp = new Necromancy(this,target);
		target.receiveMagicStatus(temp);
		stalks.add(temp);
		super.attack(target);
	}
	
	@Override
	public void castSpell(Spell spell, Unit target) throws UnitIsDeadException, FailedSpellRequirementsException, 
		InvalidRangeException {
		target.isAlive();
		Necromancy temp = new Necromancy(this,target);
		target.receiveMagicStatus(temp);
		stalks.add(temp);		
		super.castSpell(spell, target);
	}
}
