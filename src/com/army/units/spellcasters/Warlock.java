package com.army.units.spellcasters;

import java.util.LinkedList;

import com.army.exceptions.FailedSpellRequirementsException;
import com.army.spells.Fireball;
import com.army.spells.SummonDemon;
import com.army.units.SpellCaster;
import com.army.units.SpriteURLs;
import com.army.units.Unit;
//import com.army.weapons.Rod;
import com.army.weapons.DarkBook;

public class Warlock extends SpellCaster {
	public Warlock(String name) {
		super(name, new DarkBook(), 100, 60, 0, 6, 0, 0 );
		summonList = new LinkedList<Unit>();
		try {
			addSpell(new Fireball());
			addSpell(new SummonDemon());
//			addSpell(new Heal());
		}
		catch (FailedSpellRequirementsException e) {
			System.out.println("Contructor for warlock is not alrite :/");
		}
		setHealPower(0.3);
		setDamagePower(1.25);
		graphic = SpriteURLs.WARLOCK.graphics;
	}
}
