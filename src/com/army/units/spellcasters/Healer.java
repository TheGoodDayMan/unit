package com.army.units.spellcasters;

import com.army.exceptions.FailedSpellRequirementsException;
//import com.army.spells.Fireball;
import com.army.spells.Heal;
import com.army.spells.Spell;
import com.army.spells.SpellNames;
import com.army.units.SpellCaster;
import com.army.weapons.Rod;

public class Healer extends SpellCaster {
	public Healer(String name) {
		super(name, new Rod("Wizzard's rod"), 100, 60, 4, 2, 0, 0 );
		try {
			addSpell(new Heal());
			//addSpell(new Fireball());
		}
		catch (FailedSpellRequirementsException e) {
			System.out.println("Contructor for healer is not alrite :/");
		}
		setHealPower(1);
		setDamagePower(0.5);
	}
}
