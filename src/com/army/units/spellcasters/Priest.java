package com.army.units.spellcasters;

import com.army.exceptions.FailedSpellRequirementsException;
import com.army.exceptions.InvalidRangeException;
import com.army.exceptions.UnitIsDeadException;
import com.army.spells.DamagingSpell;
import com.army.spells.Spell;
import com.army.units.Unit;
import com.army.units.melee.Vampire;
import com.army.units.spellcasters.Healer;

public class Priest extends Healer {
	public Priest(String name) {
		super(name);
	}
	
	@Override
	public void castSpell(Spell spell, Unit target) throws UnitIsDeadException, FailedSpellRequirementsException, 
		InvalidRangeException { 
		if (spell instanceof DamagingSpell && (target instanceof Vampire || target instanceof Necromancer)) {
			setDamagePower(getDamagePow()*2);
			super.castSpell(spell, target);
			setDamagePower(getDamagePow()/2);
		} else { //could as well be an effect that is applied and removed from target, but I need this done quick.
			super.castSpell(spell, target);
		}
//		
	}
}
