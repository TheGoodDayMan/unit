package com.army.units.long_range;

import com.army.units.SpriteURLs;
import com.army.units.Unit;
import com.army.weapons.Bow;

public class Archer extends Unit{
	public Archer(String name) {
		super(name, new Bow("Archer's bow"), 120);
		graphic = SpriteURLs.ARCHER.graphics;
	}
}
