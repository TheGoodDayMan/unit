package com.army.units;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import com.army.CheckedConsumer;
import com.army.Location;
import com.army.exceptions.*;
import com.army.spells.DamagingSpell;
import com.army.spells.HealingSpell;
import com.army.spells.Spell;
import com.army.spells.SpellNames;
import com.army.status_effects.Reasons;
import com.army.units.Unit.Actions;
import com.army.weapons.Weapon;

public class SpellCaster extends Unit {
	protected Map<SpellNames, Spell> spellBook;
	protected int White;
	protected int Black;
	protected int Green;
	protected int Blue;
	protected double mana; //Separate from status? oh well... Will refactor?
	protected double maxMana;
	protected float healPower; //relative , 1 is full heal power. 0.5 is half of it.
	protected float damagePower; //relative
	protected List<Unit> summonList; //Everything can summon with enough potent magic. But eh, not yet in current power level.
	//Only warlock will "new" this list.
	
	//Spells?
	
	public SpellCaster(String name, Weapon wpn, double mhp, double mp, int W, int B, int G, int Bl) {
		super(name, wpn, mhp);
		graphic = SpriteURLs.WIZARD.graphics; 
//		summonList = new LinkedList<Unit>();
		spellBook = new HashMap<SpellNames, Spell>();
		
		maxMana = mp;
		mana = maxMana;
		
		White = W;
		Black = B;
		Green = G;
		Blue = Bl;
	}
	
	public float getHealPow() {
		return (float) healPower;
	}
	
	public float getDamagePow() {
		return (float) damagePower;
	}
	
	public int getWhite() {
		return White;
	}

	public int getBlack() {
		return Black;
	}

	public int getGreen() {
		return Green;
	}

	public int getBlue() {
		return Blue;
	}
	
	public double getMP() {
		return mana;
	}
	
	public double getMaxMP() {
		return maxMana;
	}
	
	public List<Unit> getSummonList() {
		return summonList;
	}
	
	public void setHealPower(double d) {
		healPower = (float)d;
	}
	
	public void setDamagePower(double dP) {
		damagePower = (float)dP;
	}
	
	public void setMana(double value) {
		if (value > maxMana)
			mana = maxMana;
		else if (value < 0)
			mana = 0;
		else 
			mana = value;
	}
	
	public void initSummonList() {
		summonList = new LinkedList<Unit>();
	}
	
	public double considerPower(Spell spell, double value) {
		if (spell instanceof DamagingSpell) {
			value *= damagePower;
		} else if (spell instanceof HealingSpell) {
			value *= healPower;
		}
		return value;
	}
	
	
	public Map<SpellNames, Spell> getBook() {
		return spellBook;
	}
	
	public Map<String, CheckedConsumer<Location>> getCommands() {
		Map<String, CheckedConsumer<Location>> menu = super.getCommands();
		for (Entry<SpellNames, Spell> spell: spellBook.entrySet()) {
			menu.put(spell.getKey().name(), (x) -> {
				if(x.getUnit() instanceof Mountable) {
					field.getMountPickList(x.getUnit(), (y) -> {this.castSpell(spell.getValue(), y);} );
				} else if(x.getUnit()!=null)
					this.castSpell(spell.getValue(), x.getUnit());
				});
		}
		return menu;
	}
	
	public String bookAsString() {
		StringBuilder sb = new StringBuilder("Spell book : \n");
		spellBook.forEach((x, y) -> {
			sb.append(x);
			sb.append(" --- ");
			sb.append(y.getReqsAsString());
			sb.append("\n");
			});
		return sb.toString();
	}
	
	public void addSpell(Spell spell) throws FailedSpellRequirementsException {
		if (spell.affinitiesSatisfied(this)) {
			spellBook.put(spell.getNameInBook(), spell);
		} else {
			throw new FailedSpellRequirementsException(this.getName());
		}
	}
	
	public Spell getSpell(SpellNames sn) {
		return spellBook.get(sn);
	}
	
	public void castSpell(Spell spell, Unit target) throws UnitIsDeadException, FailedSpellRequirementsException,
		InvalidRangeException {
		if(spellBook.containsKey(spell.getNameInBook()) && spell.affinitiesSatisfied(this) 
				&& mana > spell.getManaCons()) {
			stats.considerEffects(Reasons.ACTION);
			isAlive();
			target.isAlive();
			stats.considerEffects(Reasons.CAST);
			setMana(mana - spell.getManaCons());
			spell.cast(this, target);
		} else {
			throw new FailedSpellRequirementsException(this.getName());
		}
	}
	
	
}
