package com.army.units;

import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

public enum SpriteURLs {
	WARLOCK("https://i.imgur.com/5MsxOPM.png"),
	WEREWOLF_MAN("https://i.imgur.com/KI0pv3C.png"),
	WIZARD("https://i.imgur.com/Ub0nlf6.png"),
	WEREWOLF("https://i.imgur.com/MzhZMKb.png"),
	WARRIOR("https://i.imgur.com/0BF1HUC.png"),
	ARCHER("https://i.imgur.com/T2P5Q6m.png"),
	VAMPIRE("https://i.imgur.com/AWzrcbK.png"),
	NECROMANCER("https://i.imgur.com/RcR5N3c.png"),
	HORSE("https://i.imgur.com/eJ8uQYm.png"),
	GROUND("https://i.imgur.com/WTsSWnz.png");
	
	public URL url;
	public Image graphics;
	
	SpriteURLs(String arg0){
		try {
			url = new URL(arg0);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		try {
			graphics = ImageIO.read(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
