package com.army.units.mountable;

import com.army.units.Mountable;
import com.army.units.SpriteURLs;

public class Horse extends Mountable {
	protected static final double DEFAULT_RANGE = 2;
	protected static final double DEFAULT_HP = 75;
	protected static final int DEFAULT_MAX_MOUNTS = 1;
	
	public Horse(String name) {
		super(name, DEFAULT_HP, DEFAULT_RANGE, DEFAULT_MAX_MOUNTS);
		graphic = SpriteURLs.HORSE.graphics;
	}
}
