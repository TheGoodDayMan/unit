package com.army.units;

import com.army.exceptions.UnitIsDeadException;
import com.army.status_effects.*;
import java.util.Set;
import java.util.stream.Stream;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Status
 * @author pavlo
 * @Contains:
 *	HP of a unit, status aliments.
 *	Methods: HP can be changed, aliments can be placed, aliments will be considered after specific action (in ENUM)
 *	For example: Poison will remove a small percentage of HP after an action is taken, necromancy will check if HP is 0
 *	and give a percentage of maxHP to respective necromancer. Each status will belong to a specific group shown in enum,
 *	and will be called after respective action. (For example, necromancy has Reasons.DEATH. Poison has Eeasons.ACTION)
 */
public class Status {
	private double maxHP;
	private double health;
	private Set<StatusEffect> effects;
	private Unit onUnit;
	
	public Status(double mhp, Unit onIt) {
		effects = new HashSet<StatusEffect>();
		
		maxHP = mhp;
		
		health = maxHP;
		
		onUnit = onIt;
	}
	
	public double getHP() {
		return health;
	}
	
	public double getMaxHP() {
		return maxHP;
	}
	
	public void setHP(double newHP) throws UnitIsDeadException {
		if (newHP > maxHP)
			health = maxHP;
		else if (newHP < 0)
			health = 0;
		else 
			health = newHP;
		
		try {
			onUnit.isAlive();
		}
		catch (UnitIsDeadException e) {
			considerEffects(Reasons.DEATH);
			throw e;
		}
	}
	
	public void setMaxHP(double newMaxHP) {
		health *= newMaxHP / maxHP;
		maxHP = newMaxHP;
	}
	
	public void addEffect(StatusEffect se) {
		effects.add(se);
	}
	
	public Unit whose() {
		return onUnit;
	}
	
	public void considerEffects(Reasons r) throws UnitIsDeadException {
		Iterator<StatusEffect> it = effects.iterator();
		StatusEffect eff = null;
		try {
//			if (eff.getReason() == r) {
//				eff.takeEffect(this);
//			}
			while (it.hasNext()) {
				eff = it.next();
				if (eff.getReason() == r) {
					eff.takeEffect(this);
				}
			}
		} catch (UnitIsDeadException e) {
			if(eff.getReason() != Reasons.DEATH) {
				considerEffects(Reasons.DEATH);
			}
		}
	}
}
