package com.army.units;
/** Unit
 * 
 * @author pavlo
 *
 * @Contains 
 *	status object that contains HP and applied status aliments.
 *	weapon that contains range, damage.
 *	Methods: can deal damage, receive damage, equip weapon, check if he, himself, is alive. 
 */

import com.army.weapons.*;
import com.army.status_effects.Reasons;
import com.army.status_effects.StatusEffect;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.army.CheckedBiConsumer;
import com.army.CheckedConsumer;
import com.army.FieldWindow;
import com.army.Location;
import com.army.exceptions.*;
import com.army.spells.Spell;

public abstract class Unit implements Cloneable{ //Cloneable --- Just for summons;
	//GUI/Location properties
	protected Image graphic;
	protected Location onCell;
	
	//Unit itself's properties
	protected Status stats;
	protected String unitName;
	protected double magicResistance = 0;
	protected double moveRange = 1;
	
	//Other classes as unit's properties
	protected Weapon wpn; //equipped weapon
	protected Mountable mounts;
	protected static FieldWindow field;
	
	public Unit(String name, Weapon weapon, double mhp) {
		try {
			//Image tempGraphic = ImageIO.read(new File(
			//"/home/pavlo/LITS-Eclipse/Army/bin/com/army/units/Everyone until I find the sprite.jpg"));
			Image tempGraphic = SpriteURLs.WARRIOR.graphics;
			this.graphic = tempGraphic;
		} catch(Exception ioe) {
			graphic = null;
		}
		stats = new Status(mhp, this);
		unitName = name;
		wpn = weapon;
	}
	
	protected enum Actions {
		ATTACK((x,y) -> {
			if (y.getUnit() instanceof Mountable) {
				field.getMountPickList(y.getUnit(), (z) -> {
					x.attack(z);
				});
			} else if (y.getUnit() != null)
				x.attack(y.getUnit());
		}),
		MOVE((x,y) -> { 
			if (y.getUnit() == null) {
				x.move(y);
				}
		}),
		
		MOUNT((x,y) -> {
			if (y.getUnit() != null)
				x.mount(y.getUnit());
		}),
		
		UNMOUNT((x,y) -> {
			x.unMount();
		});
		
		Actions(CheckedBiConsumer<Unit, Location> act) {
			executor = act;
		}
		
		CheckedBiConsumer<Unit, Location> executor;
	}
	
	public String getName() {
		return unitName;
	}
	
	public Weapon getWeapon() {
		return wpn;
	}
	
	public Status getStats() {
		return stats;
	}
	
	public Mountable getMount() {
		return mounts;
	}
	
	public Location getLocation() {
		if(mounts != null) {
			return mounts.getLocation();
		} else {
			return onCell;
		}
	}
	
	public Image getGraphic() {
		return graphic;
	}
	
	public Map<String, CheckedConsumer<Location>> getCommands() {
		Map<String, CheckedConsumer<Location>> menu = new HashMap<String, CheckedConsumer<Location>>();
		for (Actions entry : Actions.values()) {
			menu.put(entry.name(),(target)->{entry.executor.accept(this,target);});
		}
//		menu.put(Actions.ATTACK.name(),(target)->{Actions.ATTACK.executor.accept(this,target);});
//		menu.put(Actions.MOVE.name(),(target)->{Actions.MOVE.executor.accept(this,target);});
		return menu;
	} //TODO
	
	public static void setWindow(FieldWindow fw) {
		field = fw;
	}
	
	public void setWeapon(Weapon wpn) {
		this.wpn = wpn;
	}
	
	public void setName(String name) {
		this.unitName = name;
	}
	
	public void setLocation(Location newLoc) {
		if (onCell != null) onCell.setUnit(null);
		onCell = newLoc;
		if (onCell != null) onCell.setUnit(this);
	}
	
	public void setMount(Mountable m) {
		mounts = m;
	}
	
	public Weapon giveWeapon() {
		Weapon retWPN = wpn; 
		wpn = null;
		return retWPN;
	}
	
	public void isAlive() throws UnitIsDeadException {
		if (stats.getHP() == 0) {
			throw (new UnitIsDeadException(unitName));
		}
	}
	
	public void move(Location loc) throws InvalidRangeException, InvalidUnmountException, UnitIsDeadException {
		isAlive();
		if (getLocation().distanceTo(loc) > moveRange) {
			throw new InvalidRangeException(moveRange, getLocation().distanceTo(loc));
		} else {
			if (mounts != null) {
				unMount();
			}
			setLocation(loc);
		}
	}
	
	public void mount(Unit u) throws InvalidUnitMountTypeException {
		if(u instanceof Mountable) {			
			if (((Mountable) u).getMounted(this)) {
				setLocation(null);
				mounts = (Mountable) u;
			}
		} else {
			throw new InvalidUnitMountTypeException(u, this);
		}
	}
	
	public void unMount() throws InvalidUnmountException{
		if (mounts != null) {
			Location locationToDeploy = this.getLocation().nearAndEmpty();
			
			if(locationToDeploy != null) {
				mounts.getUnmounted(this);
				mounts = null;
				setLocation(locationToDeploy);
			} else {
				throw new InvalidUnmountException(this, mounts);
			}
		}
	}
	
	public void attack(Unit target) throws UnitIsDeadException, InvalidRangeException { 
		//TODO Out as separate mechanic of damage (separate class) 
		try {
		wpn.getDamage();
		stats.considerEffects(Reasons.ACTION);
		isAlive();
		target.isAlive();
		if (wpn.getRange() < getLocation().distanceTo(target.getLocation())) {
			throw new InvalidRangeException(wpn.getRange(), getLocation().distanceTo(target.getLocation()));
		}
		stats.considerEffects(Reasons.ATTACK);
		target.takeDamage(wpn.getDamage(), this); //This method allows melee units to counter-attack. will be appended 
		} catch (NullPointerException e) {
			System.out.println("Apparently, someone doesn't have a weapon");
			stats.considerEffects(Reasons.ACTION);
			isAlive();
			target.isAlive();
			if (1 < getLocation().distanceTo(target.getLocation())) {
				throw new InvalidRangeException(wpn.getRange(), getLocation().distanceTo(target.getLocation()));
			}
			stats.considerEffects(Reasons.ATTACK);
			target.takeDamage(3, this); //Brawl does 3 damage now. Also if you reference a null unit, 
			//you'll screw this sequence up
		}
	}
	
	public void takeDamage(double damage, Unit from) throws UnitIsDeadException, InvalidRangeException {
		stats.setHP(stats.getHP()-damage); //stops there if dies, probably
		if (wpn != null && wpn.getRange() >= getLocation().distanceTo(from.getLocation())) {
			stats.considerEffects(Reasons.ACTION);
			stats.considerEffects(Reasons.ATTACK);
			from.getStats().setHP(from.getStats().getHP()- (wpn.getDamage()/2));
		}
	}
	
	public void takeMagicDamage(double damage) throws UnitIsDeadException {
		damage = damage * (1-magicResistance);
		stats.setHP(stats.getHP()-damage);
	}
	
	public void receiveMagicHeal(double undamage) throws UnitIsDeadException {
		//damage = damage * (1-magicResistance);
		stats.setHP(stats.getHP()+undamage);
	}
	
	public void receiveMagicStatus(StatusEffect effect) {
		getStats().addEffect(effect);
	}
	
	public double considerMagicResistance(Spell spell, double effect) {
		effect *= (1-magicResistance);
		return effect;
	}
	
	@Override
	public String toString() {
		return (new StringBuilder()
					.append(getName())
					.append(": HP = ")
					.append(getStats().getHP())
					.append("/")
					.append(getStats().getMaxHP())
					.append("; Weapon = ")
					.append(getWeapon().getName())
					.append(", deals ")
					.append(getWeapon().getDamage())
					.append(" damage")
					.toString()
					);
	}

	public Object clone() throws CloneNotSupportedException{  
		return super.clone();  
	}  
	//public Weapon getWeapon() {} // setWeapon(), too.
	
	
}
