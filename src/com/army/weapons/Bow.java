package com.army.weapons;

public class Bow extends Weapon {
	public final static double BOW_RANGE = 3;
	public final static double BOW_DEFAULT_DAMAGE = 25;
	
	public Bow(String name) {
		super(name, BOW_DEFAULT_DAMAGE,BOW_RANGE);
		// TODO Auto-generated constructor stub
	}

	public Bow(String name, double damage) {
		super(name, damage, BOW_RANGE);
		// TODO Auto-generated constructor stub
	}

}
