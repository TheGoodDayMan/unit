package com.army.weapons;

public class Rod extends Weapon {
	
	final static double ROD_RANGE = 1;
	final static double ROD_BASE_DAMAGE = 10;
	
	public Rod(String name) {
		super(name, ROD_BASE_DAMAGE,ROD_RANGE);
	}
	
	public Rod(String name,double damage) {
		super(name, damage,ROD_RANGE);
	}
}
