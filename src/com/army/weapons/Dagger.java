package com.army.weapons;

public class Dagger extends Weapon {
	final static double DAGGER_RANGE = 1;
	final static double DAGGER_BASE_DAMAGE = 20;
	public Dagger(String name) {
		super(name, DAGGER_BASE_DAMAGE ,DAGGER_RANGE);
	}
	
	public Dagger(String name, double damage) {
		super(name, damage,DAGGER_RANGE);
	}
}
