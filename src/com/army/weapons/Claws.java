package com.army.weapons;

public class Claws extends Weapon {
	final static double CLAWS_RANGE = 1;
	final static double CLAWS_BASE_DAMAGE = 25;
	
	public Claws() {
		super("Claws", CLAWS_BASE_DAMAGE, CLAWS_RANGE);
	}
	
	public Claws(String name) {
		super(name, CLAWS_BASE_DAMAGE, CLAWS_RANGE);
	}
	
	public Claws(String name, double damage) {
		super(name, damage, CLAWS_RANGE);
	}
}
