package com.army.weapons;

public class Sword extends Weapon {
	
	final static double SWORD_RANGE = 1;
	final static double SWORD_BASE_DAMAGE = 35;
	
	public Sword(String name) {
		super(name, SWORD_BASE_DAMAGE, SWORD_RANGE);
	}
	
	public Sword(String name, double damage) {
		super(name, damage,SWORD_RANGE);
	}
}
