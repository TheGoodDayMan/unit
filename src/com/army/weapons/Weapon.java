package com.army.weapons;

public abstract class Weapon {
	protected String name;
	protected double damage;
	protected double range;
	
	public Weapon(String name, double damage, double range) {
		this.name = name;
		this.damage = damage;
		this.range = range;
	}
	
	public String getName() {
		return name;
	}
	
	public double getDamage() {
		return damage;
	}
	
	public double getRange() {
		return range;
	}
	
	public String toString() {
		return String.format("%s : deals %f physical damage.", name, damage);
	}
}
