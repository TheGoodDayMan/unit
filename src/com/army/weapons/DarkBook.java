package com.army.weapons;

public class DarkBook extends Weapon {
	final static double DARK_ENERGY_RANGE = 2;
	final static double DARKBOOK_BASE_DAMAGE = 5;
	public DarkBook() {
		super("DarkBook", DARKBOOK_BASE_DAMAGE, DARK_ENERGY_RANGE );
	}
	
	public DarkBook(String name) {
		super(name, DARKBOOK_BASE_DAMAGE, DARK_ENERGY_RANGE );
	}
	
	public DarkBook(String name, double damage) {
		super(name, damage, DARK_ENERGY_RANGE );
	}
}
