package com.army.weapons;

public class Axe extends Weapon {

	final static double AXE_RANGE = 1;
	final static double AXE_BASE_DAMAGE = 30;
	
	public Axe(String name) {
		super(name, AXE_BASE_DAMAGE, AXE_RANGE);
	}
	
	public Axe(String name, double damage) {
		super(name, damage, AXE_RANGE);
	}
}
