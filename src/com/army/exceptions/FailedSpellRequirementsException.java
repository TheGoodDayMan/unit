package com.army.exceptions;

public class FailedSpellRequirementsException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4643666690419403301L;

	public FailedSpellRequirementsException(String name) {
		super(String.format("The unit %s cannot cast this spell!", name));
	}
}
