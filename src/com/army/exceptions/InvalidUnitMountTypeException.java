package com.army.exceptions;

import com.army.units.Unit;

public class InvalidUnitMountTypeException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6468842784172044576L;

	public InvalidUnitMountTypeException(Unit target, Unit mounter) {
		super(String.format("Cannot mount %s as %s", target.getClass().getSimpleName(), 
				mounter.getClass().getSimpleName()));
	}
}
