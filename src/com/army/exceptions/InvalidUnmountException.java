package com.army.exceptions;

import com.army.units.Unit;

public class InvalidUnmountException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4110132545620998897L;

	public InvalidUnmountException(Unit mounter, Unit mount) {
		super(String.format("Cannot unmount %s as %s", mount.getClass().getSimpleName(), 
				mounter.getClass().getSimpleName()));
	}
}
