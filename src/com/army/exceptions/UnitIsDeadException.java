package com.army.exceptions;

public class UnitIsDeadException extends Exception {
	/** "Unit is dead" exception
	 * is Thrown within a void isAlive method, or in takeDamage method. Is used to handle situations where a unit is 
	 * trying to attack a dead unit (to NOT beat a corpse, as the task said), or when a unit dies (to trigger On-Death 
	 * effects).
	 */
	private static final long serialVersionUID = 2858709928017539609L;

	public UnitIsDeadException(String name) {
		super(String.format("The unit %s is already dead!", name));
	}
}
