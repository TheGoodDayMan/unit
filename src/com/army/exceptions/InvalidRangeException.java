package com.army.exceptions;

public class InvalidRangeException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7919169382486081144L;

	public InvalidRangeException(double range, double targetRange) {
		super(String.format("Target at %f units away is unreachable! Your range: %f", targetRange, range));
	}
}
